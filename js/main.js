$(document).ready(function() {
	//count display
	$('fieldset').each(function() {
		if(!$('legend', this).hasClass('skip')) {
			var legend = $('legend', this).text();
			var count = legend.charAt(legend.length-1);
			var children = $(this).children('img').length;
			//compairison
			if(count==children && children>0) $(this).addClass('complete');
			//ajout de l'affichage du compte des images en place
			var newlegend = legend.replace(count, children+'/'+count);
			$('legend', this).text(newlegend);
		}
	});

	//we load smaller versions of the thumbnails and link to bigger versions
	//img managements
	$('fieldset img').each(function() {
		var link = $(this).attr('src');
		link = link.replace('.jpg','_big.jpg');
		var alt = $(this).attr('alt');
		var rel = $(this).siblings('legend').text();
		var title = rel;
		if(alt!=undefined) title = rel + " - " + alt;
		//console.log(rel);
		$(this).wrap('<a href="'+link+'" class="fancybox" rel="'+rel+'" title="'+title+'" />');
	});

	//action sur les liens
	$('a.fancybox').fancybox();

	//on monte un sommaire dynamique!
	counter = 0;
	$('fieldset legend').each(function() {
		if(!$(this).hasClass('skip')) {
			counter++;
			$('#sommaire').append('<li><a href="#'+counter+'">'+$(this).text()+'</a></li>');
			//anchor to jump right the that entry
			$(this).before('<a name="'+counter+'"></a>');
			if($(this).parent().hasClass('complete')) {
				$('#sommaire a:last').addClass('complete');
			}
		}
	});
	$('#sommaire a').click(function() {
		$('#sommaire a.selected').removeClass('selected');
		$(this).addClass('selected');
	});
	
	//I also add a link to go back to the top
	$("body").prepend("<a href='#' id='topArrow'>TOP</a>");
});